/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Anaconda extends Model
{
    public Anaconda()
    {
        super("Anaconda");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.480000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.003", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.640000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.001", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.320000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.004", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.800000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.112000f, 0.112000f,
                0.112000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[2]), new Face(0, 2, 3, 1, 1, 1, mMaterials[2]),
            new Face(0, 3, 4, 2, 2, 2, mMaterials[2]), new Face(5, 6, 7, 3, 3, 3, mMaterials[0]),
            new Face(5, 7, 3, 4, 4, 4, mMaterials[0]), new Face(5, 3, 2, 5, 5, 5, mMaterials[0]),
            new Face(8, 9, 5, 6, 6, 6, mMaterials[0]), new Face(8, 5, 2, 7, 7, 7, mMaterials[0]),
            new Face(8, 2, 1, 8, 8, 8, mMaterials[0]), new Face(10, 11, 8, 9, 9, 9, mMaterials[1]),
            new Face(10, 8, 1, 10, 10, 10, mMaterials[1]), new Face(1, 0, 10, 10, 10, 10, mMaterials[1]),
            new Face(12, 13, 10, 11, 11, 11, mMaterials[0]), new Face(10, 0, 12, 12, 12, 12, mMaterials[0]),
            new Face(12, 0, 4, 13, 13, 13, mMaterials[0]), new Face(12, 4, 3, 14, 14, 14, mMaterials[0]),
            new Face(12, 3, 7, 15, 15, 15, mMaterials[0]), new Face(12, 7, 14, 16, 16, 16, mMaterials[0]),
            new Face(6, 14, 7, 17, 17, 17, mMaterials[4]), new Face(6, 5, 9, 18, 18, 18, mMaterials[3]),
            new Face(6, 9, 11, 19, 19, 19, mMaterials[3]), new Face(8, 11, 9, 20, 20, 20, mMaterials[1]),
            new Face(10, 13, 11, 21, 21, 21, mMaterials[1]), new Face(13, 12, 14, 22, 22, 22, mMaterials[3]),
            new Face(13, 14, 11, 23, 23, 23, mMaterials[3]), new Face(6, 11, 14, 24, 24, 24, mMaterials[4])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(2.66666666666667f, 4.6974358974359f, 0.307692307692308f),
            new Vector3f(-2.66666666666667f, 4.6974358974359f, 0.307692307692308f),
            new Vector3f(-4.41025641025641f, 1.21025641025641f, 3.7948717948718f),
            new Vector3f(-0f, -0.841025641025641f, 5.94871794871795f),
            new Vector3f(4.41025641025641f, 1.21025641025641f, 3.7948717948718f),
            new Vector3f(-7.07692307692308f, -1.66153846153846f, 1.53846153846154f),
            new Vector3f(-4.41025641025641f, -5.55897435897436f, 2.35897435897436f),
            new Vector3f(-0f, -5.04615384615385f, 5.02564102564103f),
            new Vector3f(-4.41025641025641f, 3.87692307692308f, -4.1025641025641f),
            new Vector3f(-7.07692307692308f, -0.0205128205128205f, -3.28205128205128f),
            new Vector3f(4.41025641025641f, 3.87692307692308f, -4.1025641025641f),
            new Vector3f(-0f, -0.123076923076923f, -26.0512820512821f),
            new Vector3f(7.07692307692308f, -1.66153846153846f, 1.53846153846154f),
            new Vector3f(7.07692307692308f, -0.0205128205128205f, -3.28205128205128f),
            new Vector3f(4.41025641025641f, -5.55897435897436f, 2.35897435897436f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(0f, 0.707107f, 0.707107f), new Vector3f(-0.008357f, 0.715511f, 0.698551f),
            new Vector3f(0.013278f, 0.710356f, 0.703717f), new Vector3f(-0.496978f, -0.160503f, 0.852791f),
            new Vector3f(-0.504195f, -0.18516f, 0.843506f), new Vector3f(-0.502273f, -0.195437f, 0.842334f),
            new Vector3f(-0.789049f, 0.581555f, 0.197976f), new Vector3f(-0.790576f, 0.580181f, 0.195905f),
            new Vector3f(-0.782097f, 0.590407f, 0.199358f), new Vector3f(0f, 0.983796f, -0.17929f),
            new Vector3f(0f, 0.98313f, -0.182908f), new Vector3f(0.78905f, 0.581555f, 0.197976f),
            new Vector3f(0.786019f, 0.58425f, 0.202054f), new Vector3f(0.789288f, 0.584073f, 0.189429f),
            new Vector3f(0.502273f, -0.195437f, 0.842334f), new Vector3f(0.504195f, -0.18516f, 0.843506f),
            new Vector3f(0.496978f, -0.160503f, 0.852791f), new Vector3f(-0f, -0.982006f, 0.188847f),
            new Vector3f(-0.78905f, -0.581555f, -0.197976f), new Vector3f(-0.761284f, -0.604766f, -0.233891f),
            new Vector3f(-0.821856f, 0.508063f, -0.25773f), new Vector3f(0.821856f, 0.508063f, -0.25773f),
            new Vector3f(0.789049f, -0.581555f, -0.197976f), new Vector3f(0.761284f, -0.604766f, -0.233891f),
            new Vector3f(-0f, -0.982183f, -0.187927f)
        };
        return narr;
    }
}
