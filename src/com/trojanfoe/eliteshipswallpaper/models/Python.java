/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Python extends Model
{
    public Python()
    {
        super("Python");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material.003", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.480000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.004", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.800000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.005", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.640000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.006", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.480000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.640000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.800000f, 0.800000f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.007", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.160000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[2]), new Face(1, 0, 3, 1, 1, 1, mMaterials[1]),
            new Face(0, 2, 4, 2, 2, 2, mMaterials[3]), new Face(3, 0, 4, 3, 3, 3, mMaterials[6]),
            new Face(2, 1, 5, 4, 4, 4, mMaterials[1]), new Face(5, 1, 3, 5, 5, 5, mMaterials[2]),
            new Face(2, 6, 4, 6, 6, 6, mMaterials[6]), new Face(4, 6, 3, 7, 7, 7, mMaterials[3]),
            new Face(5, 7, 8, 8, 8, 8, mMaterials[0]), new Face(5, 8, 2, 9, 9, 9, mMaterials[0]),
            new Face(3, 9, 7, 10, 10, 10, mMaterials[4]), new Face(3, 7, 5, 11, 11, 11, mMaterials[4]),
            new Face(6, 10, 9, 12, 12, 12, mMaterials[0]), new Face(6, 9, 3, 13, 13, 13, mMaterials[0]),
            new Face(2, 8, 10, 14, 14, 14, mMaterials[4]), new Face(2, 10, 6, 15, 15, 15, mMaterials[4]),
            new Face(7, 9, 10, 16, 16, 16, mMaterials[5]), new Face(7, 10, 8, 16, 16, 16, mMaterials[5])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(0f, 0f, 37.3333333333333f), new Vector3f(0f, 8f, 8f),
            new Vector3f(-16f, 0f, -2.66666666666667f), new Vector3f(16f, 0f, -2.66666666666667f),
            new Vector3f(0f, -8f, 8f), new Vector3f(0f, 8f, -5.33333333333333f),
            new Vector3f(0f, -8f, -5.33333333333333f), new Vector3f(0f, 4f, -18.6666666666667f),
            new Vector3f(-8f, 0f, -18.6666666666667f), new Vector3f(8f, 0f, -18.6666666666667f),
            new Vector3f(0f, -4f, -18.6666666666667f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(-0.549558f, 0.806018f, 0.219823f), new Vector3f(0.549558f, 0.806018f, 0.219823f),
            new Vector3f(-0.549558f, -0.806018f, 0.219823f), new Vector3f(0.549558f, -0.806018f, 0.219823f),
            new Vector3f(-0.447214f, 0.894427f, 0f), new Vector3f(0.447214f, 0.894427f, 0f),
            new Vector3f(-0.447214f, -0.894427f, -0f), new Vector3f(0.447214f, -0.894427f, 0f),
            new Vector3f(-0.431934f, 0.863868f, -0.259161f), new Vector3f(-0.46569f, 0.853766f, -0.232845f),
            new Vector3f(0.436436f, 0.872872f, -0.218218f), new Vector3f(0.466085f, 0.847427f, -0.254228f),
            new Vector3f(0.431934f, -0.863868f, -0.259161f), new Vector3f(0.46569f, -0.853766f, -0.232845f),
            new Vector3f(-0.436436f, -0.872872f, -0.218218f), new Vector3f(-0.466085f, -0.847427f, -0.254228f),
            new Vector3f(0f, 0f, -1f)
        };
        return narr;
    }
}
