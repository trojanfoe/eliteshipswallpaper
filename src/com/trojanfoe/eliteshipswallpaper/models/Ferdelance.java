/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Ferdelance extends Model
{
    public Ferdelance()
    {
        super("Ferdelance");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material.003", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.640000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.004", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.488000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.005", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.800000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.800000f, 0.800000f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.800000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.006", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.480000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.007", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.320000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[5]), new Face(0, 2, 3, 0, 0, 0, mMaterials[5]),
            new Face(1, 0, 4, 1, 1, 1, mMaterials[6]), new Face(4, 0, 5, 2, 2, 2, mMaterials[0]),
            new Face(5, 0, 6, 2, 2, 2, mMaterials[0]), new Face(7, 5, 6, 3, 3, 3, mMaterials[3]),
            new Face(7, 6, 8, 3, 3, 3, mMaterials[3]), new Face(9, 7, 8, 4, 4, 4, mMaterials[0]),
            new Face(9, 8, 2, 4, 4, 4, mMaterials[0]), new Face(9, 2, 1, 5, 5, 5, mMaterials[6]),
            new Face(0, 3, 6, 6, 6, 6, mMaterials[6]), new Face(6, 3, 8, 7, 7, 7, mMaterials[5]),
            new Face(8, 3, 2, 8, 8, 8, mMaterials[6]), new Face(9, 1, 4, 9, 9, 9, mMaterials[4]),
            new Face(9, 4, 5, 9, 9, 9, mMaterials[4]), new Face(9, 5, 7, 9, 9, 9, mMaterials[4]),
            new Face(10, 11, 12, 9, 9, 9, mMaterials[1]), new Face(13, 14, 15, 10, 10, 10, mMaterials[2]),
            new Face(16, 17, 18, 11, 11, 11, mMaterials[2])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(-8f, 2.8f, -0.8f), new Vector3f(0f, -2.8f, 21.6f), new Vector3f(8f, 2.8f, -0.8f),
            new Vector3f(0f, 3.6f, -4f), new Vector3f(-8f, -2.8f, -0.8f), new Vector3f(-2.4f, -2.8f, -10.4f),
            new Vector3f(-2.4f, 0.4f, -10.4f), new Vector3f(2.4f, -2.8f, -10.4f), new Vector3f(2.4f, 0.4f, -10.4f),
            new Vector3f(8f, -2.8f, -0.8f), new Vector3f(0f, -2.877248f, -4f), new Vector3f(2.8f, -2.877248f, 8.8f),
            new Vector3f(-2.8f, -2.877248f, 8.8f), new Vector3f(-5.2f, 1.852432f, 3.6f),
            new Vector3f(-0.608f, -2.162144f, 19.408f), new Vector3f(-3.2f, 2.845856f, -0.8f),
            new Vector3f(3.2f, 2.839824f, -0.8f), new Vector3f(0.608f, -2.168176f, 19.408f),
            new Vector3f(5.2f, 1.881872f, 3.351904f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(0f, 0.970142f, 0.242536f), new Vector3f(-0.941742f, 0f, 0.336336f),
            new Vector3f(-0.863779f, -0f, -0.503871f), new Vector3f(0f, 0f, -1f),
            new Vector3f(0.863779f, 0f, -0.503871f), new Vector3f(0.941742f, 0f, 0.336336f),
            new Vector3f(-0.235238f, 0.901744f, -0.362658f), new Vector3f(0f, 0.894427f, -0.447214f),
            new Vector3f(0.235238f, 0.901744f, -0.362658f), new Vector3f(0f, -1f, 0f),
            new Vector3f(0.036731f, 0.971073f, 0.235943f), new Vector3f(-0.027216f, 0.971088f, 0.237167f)
        };
        return narr;
    }
}
