/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Transporter extends Model
{
    public Transporter()
    {
        super("Transporter");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material.003", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.420315f,
                0.362260f, 0.739621f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.005", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.320000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.800000f, 0.739621f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.448000f,
                0.448000f, 0.448000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.006", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.454834f, 0.749017f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.007", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.800000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[1]), new Face(0, 2, 3, 0, 0, 0, mMaterials[1]),
            new Face(0, 3, 4, 0, 0, 0, mMaterials[1]), new Face(0, 4, 5, 0, 0, 0, mMaterials[1]),
            new Face(0, 5, 6, 0, 0, 0, mMaterials[1]), new Face(7, 8, 4, 1, 1, 1, mMaterials[3]),
            new Face(7, 4, 3, 1, 1, 1, mMaterials[3]), new Face(2, 7, 3, 2, 2, 2, mMaterials[3]),
            new Face(9, 10, 7, 3, 3, 3, mMaterials[5]), new Face(9, 7, 2, 3, 3, 3, mMaterials[5]),
            new Face(9, 2, 1, 3, 3, 3, mMaterials[5]), new Face(9, 1, 11, 3, 3, 3, mMaterials[5]),
            new Face(0, 11, 1, 4, 4, 4, mMaterials[3]), new Face(11, 0, 12, 5, 5, 5, mMaterials[3]),
            new Face(12, 0, 6, 5, 5, 5, mMaterials[3]), new Face(6, 5, 13, 6, 6, 6, mMaterials[5]),
            new Face(6, 13, 12, 7, 7, 7, mMaterials[5]), new Face(8, 13, 5, 8, 8, 8, mMaterials[4]),
            new Face(8, 5, 4, 9, 9, 9, mMaterials[4]), new Face(8, 14, 13, 10, 10, 10, mMaterials[0]),
            new Face(10, 14, 8, 11, 11, 11, mMaterials[3]), new Face(10, 8, 7, 12, 12, 12, mMaterials[3]),
            new Face(15, 9, 11, 13, 13, 13, mMaterials[3]), new Face(15, 11, 12, 14, 14, 14, mMaterials[3]),
            new Face(13, 15, 12, 15, 15, 15, mMaterials[0]), new Face(13, 14, 15, 16, 16, 16, mMaterials[0]),
            new Face(15, 14, 10, 17, 17, 17, mMaterials[3]), new Face(15, 10, 9, 17, 17, 17, mMaterials[3]),
            new Face(16, 17, 18, 0, 0, 0, mMaterials[2]), new Face(16, 18, 19, 0, 0, 0, mMaterials[2]),
            new Face(20, 21, 22, 3, 3, 3, mMaterials[2]), new Face(20, 22, 23, 3, 3, 3, mMaterials[2]),
            new Face(24, 25, 26, 3, 3, 3, mMaterials[2]), new Face(24, 26, 27, 3, 3, 3, mMaterials[2])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(14.0606060606061f, -1.45454545454545f, -13.6768290909091f),
            new Vector3f(12.6060606060606f, -3.87878787878788f, -13.6768290909091f),
            new Vector3f(-12.1212121212121f, -3.87878787878788f, -13.6768290909091f),
            new Vector3f(-13.5757575757576f, -1.45454545454545f, -13.6768290909091f),
            new Vector3f(-12.1212121212121f, 1.93939393939394f, -13.6768290909091f),
            new Vector3f(0f, 4.84848484848485f, -13.6768290909091f),
            new Vector3f(12.6060606060606f, 1.93939393939394f, -13.6768290909091f),
            new Vector3f(-16f, -3.87878787878788f, 4.74741333333333f),
            new Vector3f(-14.5454545454545f, -0.484848484848485f, 4.74741333333333f),
            new Vector3f(6.78787878787879f, -3.87878787878788f, 13.4746860606061f),
            new Vector3f(-6.3030303030303f, -3.87878787878788f, 13.4746860606061f),
            new Vector3f(16f, -3.87878787878788f, 4.74741333333333f),
            new Vector3f(14.5454545454545f, -0.484848484848485f, 4.74741333333333f),
            new Vector3f(0f, 2.90909090909091f, 4.74741333333333f),
            new Vector3f(-5.33333333333333f, -0.96969696969697f, 13.4746860606061f),
            new Vector3f(5.33333333333333f, -0.96969696969697f, 13.4746860606061f),
            new Vector3f(4.36363636363636f, 1.45454545454545f, -13.6816775757576f),
            new Vector3f(6.3030303030303f, -1.45454545454545f, -13.6816775757576f),
            new Vector3f(-6.3030303030303f, -1.45454545454545f, -13.6816775757576f),
            new Vector3f(-3.87878787878788f, 1.45454545454545f, -13.6816775757576f),
            new Vector3f(-7.27272727272727f, -3.95124363636364f, -7.37379878787879f),
            new Vector3f(-7.27272727272727f, -3.95124363636364f, 6.68680727272727f),
            new Vector3f(-7.75757575757576f, -3.95124363636364f, 6.68680727272727f),
            new Vector3f(-7.75757575757576f, -3.95124363636364f, -7.37379878787879f),
            new Vector3f(7.75757575757576f, -3.96109575757576f, -7.37379878787879f),
            new Vector3f(7.75757575757576f, -3.96109575757576f, 6.68680727272727f),
            new Vector3f(7.27272727272727f, -3.96109575757576f, 6.68680727272727f),
            new Vector3f(7.27272727272727f, -3.96109575757576f, -7.37379878787879f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(0f, 0f, -1f), new Vector3f(-0.916958f, 0.392982f, -0.068944f),
            new Vector3f(-0.843853f, -0.506312f, -0.177653f), new Vector3f(0f, -1f, 0f),
            new Vector3f(0.846991f, -0.508195f, -0.156025f), new Vector3f(0.918219f, 0.393522f, -0.044875f),
            new Vector3f(0.223686f, 0.969306f, 0.102032f), new Vector3f(0.226006f, 0.968595f, 0.103657f),
            new Vector3f(-0.226045f, 0.968764f, 0.101975f), new Vector3f(-0.232277f, 0.967822f, 0.096782f),
            new Vector3f(-0.218006f, 0.934311f, 0.282023f), new Vector3f(-0.664493f, 0.221498f, 0.713714f),
            new Vector3f(-0.643058f, 0.275596f, 0.714509f), new Vector3f(0.650366f, 0.325183f, 0.686498f),
            new Vector3f(0.652508f, 0.279646f, 0.704295f), new Vector3f(0.218006f, 0.934311f, 0.282023f),
            new Vector3f(0f, 0.913812f, 0.406139f), new Vector3f(0f, 0f, 1f)
        };
        return narr;
    }
}
