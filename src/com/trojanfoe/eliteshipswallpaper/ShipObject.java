/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper;

import com.trojanfoe.eliteshipswallpaper.models.*;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Object3D;
import com.trojanfoe.opengl.Vector3f;
import javax.microedition.khronos.opengles.*;
import java.util.Random;

public class ShipObject extends Object3D
{
    protected static final String TAG = "ShipObject";
    protected static final int MOVING_IN = 0;
    protected static final int MOVING_OUT = 1;
    protected static final int MOVING_WAIT = 2;
    protected static final int MOVEMENT_WAIT = 600;
    protected static final int ROTATION_WAIT = 200;

    protected Model[] mModels = new Model[]
    {
        new Cobramk3(), new Krait(), new Thargoid(), new Boa(), new Gecko(), new Moray(), new Adder(), new Mamba(),
        new Viper(), new Ferdelance(), new Cobramk1(), new Python(), new Anaconda(), new Asp(), new Sidewinder(),
        new Shuttle(), new Transporter(), new Coriolis()
    };

    protected volatile int mModelIndex = -1;
    protected Vector3f mMovementVector = new Vector3f(0.0f, 0.0f, 60.0f);
    protected Vector3f mRotationVector = new Vector3f();
    protected float mMinZ = -0.0f;
    protected float mMaxZ = -5000.0f;
    protected int mMovement;
    protected int mMovementWait;
    protected int mRotationIndex;
    protected float mRotationValue;
    protected int mRotationWait;
    protected Random mRandom;

    // Thruster material for this object. Moves from red to black and
    // back again (just red element changes)
    protected Material mThrusterMaterial = null;
    protected Vector3f mThrusterDiffuse = new Vector3f(1.0f, 0.0f, 0.0f);
    protected boolean mThrusterUp = false;
    protected boolean mPaused;
    protected boolean mRandomiseShipModel = false;

    public void init(GL10 gl)
    {
        mRandom = new Random();
        mRandom.setSeed(System.currentTimeMillis());

        if (mRandomiseShipModel)
        {
            mModelIndex = randomModelIndex();
        }
        else
        {
            mModelIndex = 0;
        }
        setModel(mModels[mModelIndex]);
        findThrusterMaterial();

        setPosition(new Vector3f(0.0f, 0.0f, mMaxZ));

        mMovement = MOVING_IN;
        mRotationVector.set(0, (mRandom.nextFloat() % 0.7f) + 0.3f);
        mRotationVector.set(1, (mRandom.nextFloat() % 0.7f) + 0.3f);
        mRotationVector.set(2, (mRandom.nextFloat() % 0.7f) + 0.3f);
        mRotationWait = ROTATION_WAIT;

        mPaused = false;
    }

    public void animate()
    {
        synchronized (mLock)
        {
            if (!mPaused)
            {
                if (!mRotationVector.isEmpty())
                {
                    if (--mRotationWait <= 0)
                    {
                        // Undo last setRotation
                        float f;
                        if (mRotationIndex >= 0)
                        {
                            f = mRotationVector.get(mRotationIndex);
                            mRotationVector.set(mRotationIndex, f - mRotationValue);
                        }

                        mRotationIndex = Math.abs(mRandom.nextInt()) % 3;
                        f = mRotationVector.get(mRotationIndex);
                        mRotationValue = (mRandom.nextFloat() % 0.7f) + 0.3f;
                        mRotationVector.set(mRotationIndex, f + mRotationValue);
                        mRotationWait = ROTATION_WAIT;
                    }

                    mRotation.add(mRotationVector);
                }

                if (!mMovementVector.isEmpty())
                {
                    switch (mMovement)
                    {
                    case MOVING_IN:
                        mPosition.add(mMovementVector);
                        if (mPosition.get(2) >= mMinZ)
                        {
                            mPosition.set(2, mMinZ);
                            mMovement = MOVING_WAIT;
                            mMovementWait = MOVEMENT_WAIT;
                        }
                        break;
                    case MOVING_OUT:
                        mPosition.sub(mMovementVector);
                        if (mPosition.get(2) <= mMaxZ)
                        {
                            mPosition.set(2, mMaxZ);
                            mMovement = MOVING_IN;
                            nextModel();
                        }
                        break;
                    case MOVING_WAIT:
                        if (--mMovementWait <= 0)
                        {
                            mMovement = MOVING_OUT;
                        }
                        break;
                    default:
                        break;
                    }

                    // Log.d(TAG, "New setPosition=" + setPosition);
                }
            }

            // Always animate the thruster material, even when mPaused
            animateThrusterMaterial();
        }
    }

    protected void findThrusterMaterial()
    {
        mThrusterMaterial = null;
        if (mModel != null)
        {
            for (Material mat : mModel.getMaterials())
            {
                if (mat.getName().equals("Thruster"))
                {
                    mThrusterMaterial = mat;
                    return;
                }
            }
        }
    }

    protected void animateThrusterMaterial()
    {
        if (mThrusterMaterial == null)
        {
            return;
        }
        float elm = mThrusterDiffuse.get(0); // R G B
        if (mThrusterUp)
        {
            if (elm > 0.93f)
            {
                mThrusterUp = false;
            }
            else
            {
                elm += 0.07f;
            }
        }
        else
        {
            if (elm < 0.07f)
            {
                mThrusterUp = true;
            }
            else
            {
                elm -= 0.07f;
            }
        }
        mThrusterDiffuse.set(0, elm); // R G B
        mThrusterMaterial.setDiffuse(mThrusterDiffuse);
    }

    public void prevModel()
    {
        synchronized (mLock)
        {
            if (mRandomiseShipModel)
            {
                mModelIndex = randomModelIndex();
            }
            else
            {
                if (--mModelIndex < 0)
                {
                    mModelIndex = mModels.length - 1;
                }
            }
            setModel(mModels[mModelIndex]);
            findThrusterMaterial();
        }
    }

    public void nextModel()
    {
        synchronized (mLock)
        {
            if (mRandomiseShipModel)
            {
                mModelIndex = randomModelIndex();
            }
            else
            {
                if (++mModelIndex >= mModels.length)
                {
                    mModelIndex = 0;
                }
            }
            setModel(mModels[mModelIndex]);
            findThrusterMaterial();
        }
    }

    protected int randomModelIndex()
    {
        int newModelIndex;
        do
        {
            newModelIndex = Math.abs(mRandom.nextInt()) % mModels.length;
        }
        while (newModelIndex == mModelIndex);
        return newModelIndex;
    }

    public void prevDrawMode()
    {
        synchronized (mLock)
        {
            if (--mDrawMode < Model.DRAW_FILLED)
            {
                mDrawMode = Model.DRAW_WIREFRAME_HIDDEN_LINE;
            }
        }
    }

    public void nextDrawMode()
    {
        synchronized (mLock)
        {
            if (++mDrawMode > Model.DRAW_WIREFRAME_HIDDEN_LINE)
            {
                mDrawMode = Model.DRAW_FILLED;
            }
        }
    }

    public void togglePause()
    {
        synchronized (mLock)
        {
            mPaused = !mPaused;
        }
    }

    public void setRandomizeShipMode(boolean randomiseShipModel)
    {
        synchronized (mLock)
        {
            mRandomiseShipModel = randomiseShipModel;
        }
    }
}
