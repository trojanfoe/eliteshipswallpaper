/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.opengl;

import android.util.*;
import javax.microedition.khronos.opengles.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;

public class Model
{
    private static final String TAG = "EliteShipsWallpaperService";

    public static final int DRAW_NONE = 0;
    public static final int DRAW_FILLED = 1;
    public static final int DRAW_WIREFRAME = 2;
    public static final int DRAW_WIREFRAME_HIDDEN_LINE = 3;

    // Material used to draw wireframes
    protected static final Material wireframeMat = new Material("wireframeMat", new Vector3f(0.8f, 0.8f, 1.0f),
        new Vector3f(0.8f, 0.8f, 1.0f), new Vector3f(0.8f, 0.8f, 1.0f), 1.0f, 1.0f);

    // Material used to draw wireframes-with-hidden-line removed
    // (must be the same as the window background colour)
    protected static final Material backgroundMat = new Material("backgroundMat", new Vector3f(0.0f, 0.0f, 0.0f),
        new Vector3f(0.0f, 0.0f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), 0.0f, 1.0f);

    // Material used to draw mNormals
    protected static final Material normalMat = new Material("normalMat", new Vector3f(1.0f, 1.0f, 0.0f), new Vector3f(
        1.0f, 1.0f, 0.0f), new Vector3f(1.0f, 1.0f, 0.0f), 0.5f, 1.0f);

    protected static String mModelPackage = null;
    protected boolean mInitted = false;
    protected String mName;
    protected Material[] mMaterials;
    protected IndexLengthPair[] mBufferInfo;
    protected FloatBuffer mVertices;
    protected FloatBuffer mNormals;

    protected static ShortBuffer makeShortBuffer(short[] arr)
    {
        ByteBuffer bb = ByteBuffer.allocateDirect(arr.length * 2);
        bb.order(ByteOrder.nativeOrder());
        ShortBuffer sb = bb.asShortBuffer();
        sb.put(arr);
        sb.position(0);
        return sb;
    }

    protected static FloatBuffer makeFloatBuffer(float[] arr)
    {
        ByteBuffer bb = ByteBuffer.allocateDirect(arr.length * 4);
        bb.order(ByteOrder.nativeOrder());
        FloatBuffer fb = bb.asFloatBuffer();
        fb.put(arr);
        fb.position(0);
        return fb;
    }

    public Model()
    {
        mInitted = false;
        mName = null;
        mMaterials = null;
        mBufferInfo = null;
        mVertices = null;
        mNormals = null;
    }

    public Model(String name)
    {
        this();
        mName = name;
    }

    public Material[] getMaterials()
    {
        return mMaterials;
    }

    protected void createBuffers(Face[] facearr, Vector3f[] vertarr, Vector3f[] normarr)
    {
        // Sort the faces by their mMaterial, in order to reduce the need to
        // change
        // to a mMaterial more than once (which I perceive as being the greatest
        // expense during rendering).
        Arrays.sort(facearr, new FaceComparator());

        float[] tempVerts = new float[facearr.length * 9]; // 3 mVertices, each
                                                           // of 3 floats
        float[] tempNorms = new float[facearr.length * 9]; // 3 mVertices, each
                                                           // of 3 floats
        mBufferInfo = new IndexLengthPair[mMaterials.length];

        Material currentMat = null;
        int matIndex = -1, i;

        for (i = 0; i < facearr.length; i++)
        {
            Face face = facearr[i];
            if (currentMat != face.mMaterial)
            {
                currentMat = face.mMaterial;
                matIndex = findMaterial(face.mMaterial);
                mBufferInfo[matIndex] = new IndexLengthPair(i * 3, 3);
            }
            else
            {
                mBufferInfo[matIndex].mLength += 3;
            }

            vertarr[face.vertex(0)].copyTo(tempVerts, (i * 9) + 0);
            vertarr[face.vertex(1)].copyTo(tempVerts, (i * 9) + 3);
            vertarr[face.vertex(2)].copyTo(tempVerts, (i * 9) + 6);

            normarr[face.normal(0)].copyTo(tempNorms, (i * 9) + 0);
            normarr[face.normal(1)].copyTo(tempNorms, (i * 9) + 3);
            normarr[face.normal(2)].copyTo(tempNorms, (i * 9) + 6);
        }

        mVertices = makeFloatBuffer(tempVerts);
        mNormals = makeFloatBuffer(tempNorms);
    }

    protected int findMaterial(Material m)
    {
        for (int i = 0; i < mMaterials.length; i++)
        {
            if (mMaterials[i] == m)
            {
                return i;
            }
        }
        return -1;
    }

    public String getName()
    {
        return mName;
    }

    /**
     * It's necessary to reset the 'init' status of the model so that the vertex and normal buffers
     * are correctly set into the GL context.
     */
    public void resetInitted()
    {
        mInitted = false;
    }

    public void init(GL10 gl)
    {
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVertices);
        gl.glNormalPointer(GL10.GL_FLOAT, 0, mNormals);
        mInitted = true;
    }

    public void draw(GL10 gl, int drawMode)
    {
        if (!mInitted)
        {
            init(gl);
        }

        switch (drawMode)
        {
        case DRAW_FILLED:
            drawFilled(gl);
            break;
        case DRAW_WIREFRAME:
            drawWireframe(gl);
            break;
        case DRAW_WIREFRAME_HIDDEN_LINE:
            drawWireframeHiddenLine(gl);
            break;
        default:
            break;
        }
    }

    protected void drawFilled(GL10 gl)
    {
        for (int i = 0; i < mMaterials.length; i++)
        {
            mMaterials[i].set(gl);
            gl.glDrawArrays(GL10.GL_TRIANGLES, mBufferInfo[i].mIndex, mBufferInfo[i].mLength);
        }
    }

    protected void drawWireframe(GL10 gl)
    {
        wireframeMat.set(gl);
        for (int i = 0; i < mMaterials.length; i++)
        {
            for (int j = 0; j < mBufferInfo[i].mLength; j += 3)
            {
                gl.glDrawArrays(GL10.GL_LINE_LOOP, mBufferInfo[i].mIndex + j, 3);
            }
        }
    }

    protected void drawWireframeHiddenLine(GL10 gl)
    {
        backgroundMat.set(gl);
        for (int i = 0; i < mMaterials.length; i++)
        {
            gl.glDrawArrays(GL10.GL_TRIANGLES, mBufferInfo[i].mIndex, mBufferInfo[i].mLength);
        }

        gl.glLineWidth(2.0f);
        drawWireframe(gl);
        gl.glLineWidth(1.0f);
    }

    public static void setModelPackage(String modelPackage)
    {
        Model.mModelPackage = modelPackage;
    }

    public static Model loadModel(String name)
    {
        Model model = null;
        if (mModelPackage != null)
        {
            name = mModelPackage + "." + name;
        }
        try
        {
            Class<?> cls = Class.forName(name);
            model = (Model) cls.newInstance();
        }
        catch (Exception e)
        {
            Log.e(TAG, "Could not load model '" + name + "' :" + e.toString());
        }
        return model;
    }
}
